#!/bin/bash

# Min(Date) looks for the lowest number in the 'Date' field
# Max(Date) looks for the highest number in the 'Date' field

# The colon after Min(Date) and Max(Date) is a rewrite rule to make the output more readable
# Without the colon you would get the field name prefixed with the name of aggregation function:
#
# Min_Date: 1971
# Max_Date: 2007

echo
recsel -p "Min(Date):Oldest_Movie_From,Max(Date):Latest_Movie_From" movies.rec
echo
